/*

  Author: Calvin Williams
  Email: cwilliams2016@my.fit.edu
  Course: Algorithms and Data Structures
  Section: 3
  Description: A system that can handle incoming patients,
  organized by severity level.
*/

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class HW4
{
  // -------------------- Priority class ----------------
  private static class Priority implements Comparable<Priority>
  {
    private int t, e;

    /**
     * Constructor.
     * @param time  the time the patient was admitted
     * @param esi   the ESI of the patient
     */
    public Priority(int time, int esi)
    {
      t = time;
      e = esi;
    }

    /**
     * Gets the time the patient was admitted.
     * @return the time the patient was admitted
     */
    public int getTime()
    {
      return t;
    }

    /**
     * Gets the patient's ESI
     * @return the patient's ESI
     */
    public int getESI()
    {
      return e;
    }

    /**
     * Overrides the compareTo method of Comparable.
     * @param p  the priority to compare to
     * @returns the greater priority
     */
    public int compareTo(Priority p)
    {
      if (t != e)
        return Integer.compare(this.e, p.e);
      return Integer.compare(this.t, p.t);
    }

    /**
     * Returns a string containting the time of admittance
     * and the patient's ESI.
     * @return see the description
     */
    public String toString()
    {
      return Integer.toString(t) + " " + Integer.toString(e);
    }
  } // --------------------------------------------------

  // -------------------- Doctor class ------------------
  public static class Doctor implements Comparable<Doctor>
  {
    private String n, p;
    private boolean b;
    private int t;

    /**
     * Constructor for the nested Doctor class.
     * @param name  the name of the doctor
     */
    public Doctor(String name, int ArrivalTime)
    {
      n = name;
      b = false;
      t = ArrivalTime;
      p = null;
    }

    /**
     * Checks if the doctor is busy at the given time.
     * If they are, b returns false. If they aren't, they
     * are flagged as busy since this will only be checked
     * if a patient needs help.
     * @return whether or not the doctor is busy.
     */
    public boolean isBusy(int time)
    {
      if (t < time)
      {
        b = false;
      }
      return b;
    }

    /**
     * Starts the treatment of a patient. Finishes treatment
     * if a patient if being treated.
     */
    public void startTreatment(int time)
    {
      /* Check if a patient is being treated, and call
         the finish treatment method below. */
      if (p != null)
        doctorFinishesTreatmentAndPatientDeparts(t, n, p);

      int patientTime = patients.min().getKey().getTime();
      int esi = patients.min().getKey().getESI();
      String name = patients.min().getValue();
      p = name; // Sets the current patient for this doctor

      if (t != 0)
      {
        System.out.println("doctorStartsTreatingPatient " + t + " " + n + " " + name);
        int free = (int) Math.pow(2, (6 - esi));
        t = free + t;
      }
      else
      {
        System.out.println("doctorStartsTreatingPatient " + patientTime + " " + n + " " + name);
        int free = (int) Math.pow(2, (6 - esi));
        t = free + patientTime;
      }

      // Calculate when the doctor will be free
      // int free = (int) Math.pow(2, (6 - esi));
      // t = free + time;
      b = true;

      // Remove patient from the heap
      patients.remove(patients.min());
    }

    /**
     * Returns when the doctor will be free.
     * @return see description
     */
    public int freeAt()
    {
      return t;
    }

    /**
     * Forces the doctors to finish their patients.
     * Used at the end of the program. Not elegant,
     * but it works.
     */
    public void forceFinish()
    {
      doctorFinishesTreatmentAndPatientDeparts(t, n, p);
      b = false;
    }
    /**
     * Overrides Comparable's compareTo method.
     * @param d  doctor to compare to
     * @returns  which doctor is free sooner
     */
    public int compareTo(Doctor d)
    {
      if (this.freeAt() > d.freeAt())
        return 1;
      else if (this.freeAt() < d.freeAt())
        return -1;
      else
        return 0;
    }
  } // --------------------------------------------------

  /* Global heap for patients and ArrayList for doctors. Global for ease
     of access. Otherwise I would have to pass it around everywhere. Bleh. */
  public static HeapAdaptablePriorityQueue<Priority, String> patients =
    new HeapAdaptablePriorityQueue<>();
  public static ArrayList<Doctor> doctors = new ArrayList<>();

  /**
   * Logs new patients into the heap.
   * @param time     the time of admittance
   * @param patient  the name of the patient
   * @param esi      the patient's ESI
   */
  public static void patientArrives(int time, String patient, int esi)
  {
    Priority priority = new Priority(time, esi);
    patients.insert(priority, patient);
  }

  /**
   * Updates the ESI of a given patient in O(n) time.
   * @param time     the time the ESI is updated
   * @param patient  the name of the patient
   * @param n_esi      the patient's new ESI
   */
  public static void updatePatientESI(int time, String patient, int n_esi)
  {
    for (int i = 0; i < patients.size(); i++)
    {
      if (patients.get(i).getValue().equals(patient))
      {
        patients.replaceKey(patients.get(i),
                            new Priority(patients.get(i).getKey().getTime(), n_esi));
        System.out.println("updatePatientESI " + time + " " + patient + " " + n_esi);
       break;
      }
    }
  }

  /**
   * Removes a patient without having been treated
   * by a doctor in O(n) time. O(1) if there is only
   * one patient.
   * @param time     the time the patient leaves
   * @param patient  the name of the patient
   */
  public static void patientDepartsAfterNurseTreatment(int time, String patient)
  {
    if (patients.size() == 1)
    {
      patients.remove(patients.get(0));
      System.out.println("patientDepartsAfterNurseTreatment " + time + " " + patient);
      return;
    }
    else
    {
      for (int i = 0; i < patients.size(); i++)
      {
        if (patients.get(i).getValue().equals(patient))
        {
          patients.remove(patients.get(i));
          System.out.println("patientDepartsAfterNurseTreatment " + time + " " + patient);
          break;
        }
        else if (i == patients.size())
        {
          System.out.println("Patient not found.");
        }
      }
    }
  }

  /**
   * Logs a new doctor arriving.
   * @param time    the time the doctor arrives
   * @param doctor  the name of the doctor
   */
  public static void doctorArrives(int time, String doctor)
  {
    doctors.add(new Doctor(doctor, time));
    System.out.println("doctorArrives " + time + " " + doctor);
  }

  /**
   * Starts treatment of a patient if any doctors are available.
   * @param time  the time to check if the doctor is free
   */
  public static void doctorStartsTreatingPatient(int time)
  {
    for (Doctor d : doctors)
    {
      if (!d.isBusy(time) && patients.size() != 0)
      {
        d.startTreatment(time);
      }
    }
  }

  /**
   * Notes that a doctor has finished treating a patient.
   * @param time     the time the doctor finished
   * @param doctor   the doctor that finished treating
   * @param patient  the patient that finished treatment
   */
  public static void doctorFinishesTreatmentAndPatientDeparts(int time, String doctor, String patient)
  {
    System.out.println("doctorFinishesTreatmentAndPatientDeparts " + time + " " +
                       doctor + " " + patient);
  }

  /**
   * Main method
   * @param args[0]  the name of the input file
   */
  public static void main(String[] args)
    throws IOException
  {
    Scanner scan = new Scanner(new File(args[0]));

    String inLine, command, patient, doctor;
    int time = 0, esi, hours, mins;
    String[] split;
    // The starting doctors
    doctors.add(new Doctor("Alice", 0)); doctors.add(new Doctor("Bob", 0));

    while(scan.hasNext())
    {
      inLine = scan.nextLine();
      split = inLine.split(" ");
      command = split[0];
      time = Integer.parseInt(split[1]);

      // Checks for a patient every loop before
      // command is parsed, for continuity reasons
      doctorStartsTreatingPatient(time);

      // Finds input function
      switch (command)
      {
        case "patientArrives":
          patient = split[2];
          esi = Integer.parseInt(split[3]);
          patientArrives(time, patient, esi);
          System.out.println("patientArrives " + time + " " + patient + " " + esi);
          break;
        case "updatePatientESI":
          patient = split[2];
          esi = Integer.parseInt(split[3]);
          updatePatientESI(time, patient, esi);
          break;
        case "patientDepartsAfterNurseTreatment":
          patient = split[2];
          patientDepartsAfterNurseTreatment(time, patient);
          break;
        case "doctorArrives":
          doctor = split[2];
          doctorArrives(time, doctor);
          break;
      }

      // Sort doctors after every iteration
      Collections.sort(doctors);
    }

    time = doctors.get(0).freeAt();

    // Check for remaining untreated patients,
    // and treat them
    while (!patients.isEmpty())
    {
      doctorStartsTreatingPatient(time);
      Collections.sort(doctors);
      time++;
    }

    // Force doctors to finish their patients
    for (int i = 0; i < doctors.size(); i++)
    {
      doctors.get(i).forceFinish();
    }

    scan.close();
  }

}
